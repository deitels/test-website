import http.server
import socketserver

HOST = "0.0.0.0"
PORT = 8080

Handler = http.server.SimpleHTTPRequestHandler

httpd = socketserver.TCPServer((HOST, PORT), Handler)
print("Serving HTTP on {0} port {1} ...".format(HOST, PORT))
print("Changed...")
httpd.serve_forever()
